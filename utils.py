import requests
import json

from data_for_test import BASE_URL, players_url, tanks_url


def url_get(method_block, get_params):
    return '{}{}/?{}'.format(BASE_URL, method_block, get_params)


# account_id get method
def acc_id(application_id, search, search_type='startswith'):
    user = requests.get(url_get(players_url, 'application_id={}&search={}&type={}'.
                                format(application_id, search, search_type))).json()
    account_id = user['data'][0]['account_id']
    return account_id


def get_custom_payloads(path_json):
    with open(path_json) as file:
        data = json.load(file)
    return data


# account_id post method
def acc_id2(payloads):
    user = requests.post('{}{}/'.format(BASE_URL, players_url), data=get_custom_payloads(payloads)).json()
    account_id = user['data'][0]['account_id']
    return account_id


def tank_info(application_id, account_id):
    return requests.get(url_get(tanks_url, 'application_id={}&account_id={}'.format(application_id, account_id))).json()


def tank_info2(application_id, account_id):
    payloads = {'application_id': application_id, 'account_id': account_id}
    return requests.post('{}{}/'.format(BASE_URL, tanks_url), data=payloads).json()


# number of tank battles
def tank_battles(tanks_info, account_id, tank_id):
    a = tanks_info['data'][str(account_id)]
    for i in a:
        if i['tank_id'] == tank_id:
            return i['statistics']['battles']


def tank_present(tanks_info, account_id, tank_id):
    a = tanks_info['data'][str(account_id)]
    for i in a:
        if i['tank_id'] == tank_id:
            return i['tank_id']
