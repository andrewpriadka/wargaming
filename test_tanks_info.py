import pytest

from utils import acc_id, tank_info, tank_battles, acc_id2, tank_info2, tank_present
from data_for_test import app_id, user_login, path_user_info


@pytest.fixture(scope="module")
def user_info():
    account_id = acc_id(app_id, user_login)
    tank = tank_info(app_id, account_id)
    yield {'account_id': account_id, 'tank': tank}


def test_one(user_info):
    """Написать тест на проверку кол-ва боев на конкретном танке (tank_id = 577).
Ожидаемый результат = 3."""
    battles = tank_battles(user_info['tank'], user_info['account_id'], 577)
    assert battles == 3


def test_two(user_info):
    """Написать тест на проверку общего кол-ва танков у игрока.
Ожидаемый результат = 4."""
    assert len(user_info['tank']['data'][str(user_info['account_id'])]) == 4


def test_three():
    """Написать тест на проверку наличия танка у игрока с tank_id = 3089.
    Желательно отсылать запрос методом POST."""
    account_id = acc_id2(path_user_info)
    tank = tank_info2(app_id, account_id)
    assert tank_present(tank, account_id, 3089) == 3089



